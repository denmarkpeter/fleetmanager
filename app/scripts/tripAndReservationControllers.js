'use strict';

angular.module('confusionApp')
.controller('TripsController', ['$scope', '$stateParams', 'tripFactory', 'groupFactory','vehicleFactory','peopleFactory','AuthFactory',
    function($scope, $stateParams,tripFactory,groupFactory, vehicleFactory,peopleFactory,AuthFactory) {
   
    $scope.userId = AuthFactory.getUserId();
    $scope.username = AuthFactory.getUsername();
    $scope.tab = 'All';
    $scope.filtText = '';
    $scope.sorting = "";

    $scope.message = "Loading ..."; 
    function tripDateConverter(trip) {
        trip.dateFrom = new Date(trip.dateFrom);
        trip.dateTo = new Date(trip.dateTo);
        return trip;
    }

    $scope.showTrips = false;
    tripFactory.query({})
    .$promise.then(
        function (response) {
            $scope.trips = response.map(tripDateConverter);
            $scope.showTrips = true;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
    });

    $scope.selfie = function(personId) {
        return personId === $scope.userId;
    }

    $scope.tripChangeStatus = function (tripId,newstatus){
        // find trip
        for (var i = 0; i < $scope.trips.length; ++i) {
            if ($scope.trips[i]._id == tripId  && $scope.trips[i].status !== newstatus) {
                $scope.trips[i].status = newstatus
                tripFactory.update({id: tripId}, $scope.trips[i]);
                break;
            }
        }
    }

    $scope.openEditTrip = function (id) {
        $scope.oldstate= $state;
        ngDialog.open({ template: 'views/trip.html', scope: $scope, className: 'ngdialog-theme-default', controller:"TripController" });
    };

    $scope.select = function(setTab) {
        $scope.tab = setTab;
        if (setTab === 'Rejected') {
            $scope.filtText = {status: 'Rejected'}
        }
        else if (setTab === 'Accepted') {
            $scope.filtText = {status: 'Accepted'}
         }
        else if (setTab === '-') {
            $scope.filtText = {status: '-'}
       }
        else 
        {
            $scope.filtText = {}
        }
    };

    $scope.isSelected = function (checkTab) {

        return ($scope.tab === checkTab);
    };

    $scope.setSorting = function(field) {
        var sortname  = ["person","purpose","group","vehicle","plate","dateFrom","dateTo","status"];
        var fieldname = ["personId.firstname","purpose","vehicleId.inGroup.name","vehicleId.brand","vehicleId.plate","dateFrom","dateTo","status"];
        
        let oldsortname = $scope.sorting;
        let index = sortname.indexOf(field);
        $scope.sorting = index === -1? "":fieldname[index];
        if (oldsortname === $scope.sorting)
           $scope.sorting = "-" + $scope.sorting
    }

}])

.controller('TripsControllerPHS', ['$scope', '$stateParams', 'tripFactoryPHS', 'groupFactory','vehicleFactory','peopleFactory','AuthFactory',
    function($scope, $stateParams,tripFactoryPHS,groupFactory, vehicleFactory,peopleFactory,AuthFactory) {
    $scope.showTrips = false;

    $scope.allMonths = [{name: "January ",number:1},{name: "February ",number:2},{name: "March    ",number:3},
                        {name: "April   ",number:4},{name: "May      ",number:5},{name: "June     ",number:6},
                        {name: "July    ",number:7},{name: "August   ",number:8},{name: "September",number:9},
                        {name: "October ",number:10},{name:"November ",number:11},{name:"December ",number:12},
                        ];

    let thisMonth = new Date().getMonth();
    let thisYear  = new Date().getFullYear();
    $scope.allMonths.map(function(x){
            x["value"] = x["number"]-1;
            x["year"]  = x["value"] > thisMonth? (thisYear-1):thisYear; 
            x["monthYear"]  =  x["name"]+"- "+x["year"]; 
            x["yearMonth"]  =  x["year"]*100 + x["value"]; 
            return x});
    $scope.selectedMonth;

    $scope.showKm = function(){
        $scope.message = "CALCULATING";
        $scope.showTrips = false;
        $scope.trips = tripFactoryPHS.query({ 
            monthNo: $scope.selectedMonth["yearMonth"]
        })
        .$promise.then(
            function(response) {
                $scope.trips = response;
                $scope.showTrips = true;
            },
            function(response) {
                $scope.message = "Error: "+response.status + " " + response.statusText;
            }
        );
    };

}])

.controller('TripController', ['$scope', '$state', '$stateParams', 'ngDialog', 'tripFactory','peopleFactory','vehicleFactory','AuthFactory',
                 function ($scope,$state,$stateParams, ngDialog, tripFactory,peopleFactory,vehicleFactory,AuthFactory) {
    $scope.showVehicles = false;
    $scope.userId = AuthFactory.getUserId();
    $scope.username = AuthFactory.getUsername();

    $scope.person = null;
    $scope.editing = false;
    $scope.selfie = null;

    function tripDateConverter(trip) {
        trip.dateFrom = new Date(trip.dateFrom);
        trip.dateTo = new Date(trip.dateTo);
    }
    
    function getPossibleVehicles(personId) {
        vehicleFactory.query({}).$promise
            .then ((response) => {
                    $scope.vehicles = response;
                   }
                ,
                function(response) {
                    $scope.message = "Error: "+response.status + " " + response.statusText;
                }
            );
    }

    if (!$stateParams.id) { 
        getPossibleVehicles($scope.userId);  // get vehicles person can reserve/use
        $scope.editing = true;
        $scope.selfie = true;
        $scope.usernameTrip = $scope.username; 


        $scope.trip = {
            purpose : "",
            dateFrom :"",
            dateTo : "",
            personCount : 1,
            personId : $scope.userId,    // can only create/edit data for own trips
            usernameTrip : $scope.username,
            vehicleId  :"",
            status: "-"
        };

        $scope.showTrips = true;
        $scope.saveTrip = function () {
            console.log("TRIP: ",$scope.trip);
            tripFactory.save($scope.trip);
            $scope.trip = {
                purpose : "",
                dateFrom :"",
                dateTo : "",
                personCount : 1,
                personId : $scope.userId,
//                usernameTrip: $scope.userId.firstname + " " + $scope.userId.familyname,
                usernameTrip : $scope.username,
                vehicleId  :"",
                status: "-"
            };
     //       $scope.location.$setPristine();
        };
    } else {
        $scope.showTrips = false;
        $scope.trip = tripFactory.get( { id: $stateParams.id } )
        .$promise.then(
            function (response) {
                $scope.trip = response;
                tripDateConverter($scope.trip)
                getPossibleVehicles($scope.userId);  // get vehicles person can reserve/use

                $scope.usernameTrip = $scope.trip.personId.firstname + " " + $scope.trip.personId.familyname;
                $scope.vehicleTrip = $scope.trip.vehicleId.brand + " " + $scope.trip.vehicleId.model + ". Plates:  " + $scope.trip.vehicleId.plate; 
                $scope.selfie = $scope.trip.personId._id === $scope.userId;
                $scope.showTrips = true;
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
            }
        );

        $scope.editTrip = function() {
            $scope.editing = true;
        }; 

        $scope.saveTrip = function() {
            tripFactory.update({id: $stateParams.id}, $scope.trip);
            $scope.cancelEditTrip();
        }; 
        $scope.deleteTrip = function() {
            tripFactory.delete({id: $stateParams.id});
            $scope.cancelEditTrip();
        }; 
        $scope.tripChangeStatus = function (status){
            if ($scope.trip && $scope.trip.status !== status) {
                $scope.trip.status = status;
                tripFactory.update({id: $stateParams.id}, $scope.trip);
                $scope.cancelEditTrip();
            }
        }
    } 
    $scope.cancelEditTrip = function() {
        if (!$stateParams.id || $scope.selfie) {
                $state.go('app.persondash',{id: $scope.userId});   // person stops looking/editing own trips
            } else {
                $state.go('app.trips', {});                        // manager stops looking at some person's trip
            }
        };
    $scope.selectVehicle = function (vehicle){
         $scope.vehicle = vehicle;
         $scope.trip.vehicleId = vehicle._id;
         $scope.vehicleTrip = vehicle.brand + " " + vehicle.model + ". Plates:  " + vehicle.plate; 
    }
                
}])

.controller('ReservationsController', ['$scope', '$stateParams', 'reservationFactory', 'groupFactory','vehicleFactory','peopleFactory',
    function($scope, $stateParams,reservationFactory,groupFactory, vehicleFactory,peopleFactory) {
   
    $scope.message = "Loading ..."; 
    $scope.sorting = "";

    function reservationDateConverter(reservation) {
        reservation.dateFrom = new Date(reservation.dateFrom);
        reservation.dateTo = new Date(reservation.dateTo);
        return reservation;
    }

    $scope.showReservations = false;
    reservationFactory.query(
        function (response) {
            $scope.reservations = response.map(reservationDateConverter);
            $scope.showReservations = true;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
    });

    $scope.selfie = function(personId) {
        return personId === $scope.userId;
    }

    $scope.openEditReservation = function (id) {
        $scope.oldstate= $state;
        ngDialog.open({ template: 'views/reservation.html', scope: $scope, className: 'ngdialog-theme-default', controller:"ReservationController" });
    };

    $scope.setSorting = function(field) {
 
        var sortname  = ["person","purpose","personCount","group","vehicle","plate","dateFrom","dateTo"];
        var fieldname = ["personId.firstname","purpose","personCount","vehicleId.inGroup.name","vehicleId.brand","vehicleId.plate","dateFrom","dateTo"];
        
        let oldsortname = $scope.sorting;
        let index = sortname.indexOf(field);
        $scope.sorting = index === -1? "":fieldname[index];
        if (oldsortname === $scope.sorting)
           $scope.sorting = "-" + $scope.sorting
    }

}])

.controller('ReservationController', ['$scope', '$state', '$stateParams', 'ngDialog', 'reservationFactory','peopleFactory','vehicleFactory','AuthFactory',
                 function ($scope,$state,$stateParams, ngDialog, reservationFactory,peopleFactory,vehicleFactory,AuthFactory) {
    $scope.showVehicles = false;
    $scope.userId = AuthFactory.getUserId();
    $scope.username = AuthFactory.getUsername();

    $scope.person = null;
    $scope.editing = false;
    $scope.selfie = null;

    function reservationDateConverter(reservation) {
        reservation.dateFrom = new Date(reservation.dateFrom);
        reservation.dateTo = new Date(reservation.dateTo);
    }
    
    function getPossibleVehicles(personId) {
        vehicleFactory.query({}).$promise
            .then ((response) => {
                    $scope.vehicles = response;
                   }
                ,

                function(response) {
                    $scope.message = "Error: "+response.status + " " + response.statusText;
                }
            );
    }

    if (!$stateParams.id)            // new reservation
    {
        $scope.editing = true;
        $scope.selfie = true;
        $scope.usernameReservation = $scope.username; 
        getPossibleVehicles($scope.userId);  // get vehicles person can reserve/use

        $scope.reservation = {
            purpose : "",
            dateFrom :"",
            dateTo : "",
            personCount : 1,
            personId : $scope.userId,
            personUsername : $scope.username,
            vehicleId  :""
        };
        $scope.usernameReservation = $scope.username;
        $scope.showreservation = true;
        $scope.saveReservation = function () {
            console.log("RESERVATION: ",$scope.reservation);
            reservationFactory.save($scope.reservation);
            $scope.reservation = {
                purpose : "",
                dateFrom :"",
                dateTo : "",
                personCount : 1,
                personId : $scope.userId,
                personUsername : $scope.username,
                vehicleId  :""
            };
     //       $scope.location.$setPristine();
        };
    } else {
        $scope.showReservation = false;
        $scope.reservation = reservationFactory.get( { id: $stateParams.id} )
        .$promise.then(
            function (response) {
                $scope.reservation = response;
                reservationDateConverter($scope.reservation)
                getPossibleVehicles($scope.userId);  // get vehicles person can reserve/use

                $scope.usernameReservation = $scope.reservation.personId.firstname + " " + $scope.reservation.personId.familyname;
                $scope.vehicleReservation = $scope.reservation.vehicleId.brand + " " + $scope.reservation.vehicleId.model + ". Plates:  " + $scope.reservation.vehicleId.plate; 
                $scope.selfie = $scope.reservation.personId._id === $scope.userId;

                $scope.showReservation = true;
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
            }
        );

        $scope.editReservation = function() {
            $scope.editing = true;
        }; 

        $scope.saveReservation = function() {
              reservationFactory.update({id: $stateParams.id}, $scope.reservation);
//                    ngDialog.close();
        }; 
        $scope.deleteReservation = function() {
              reservationFactory.delete({id: $stateParams.id});
                $scope.cancelEditReservation();
//                    ngDialog.close();
        }; 
        $scope.reservationChangeStatus = function (status){
            if ($scope.reservation && $scope.reservation.status !== status) {
                $scope.reservation.status = status;
                tripFactory.update({id: $stateParams.id}, $scope.reservation);
                $scope.cancelEditReeservation();
            }
        }
    } 

    $scope.cancelEditReservation = function() {
        if (!$stateParams.id || $scope.selfie) {
            $state.go('app.persondash',{id: $scope.userId});   // person stops looking/editing own trips
        } else {
            $state.go('app.reservations', {});                        // manager stops looking at some person's trip
        }
    };
    $scope.selectVehicle = function (vehicle){
         $scope.vehicle = vehicle;
         $scope.reservation.vehicleId = vehicle._id;
         $scope.vehicleReservation = vehicle.brand + " " + vehicle.model + ". Plates:  " + vehicle.plate; 
    }
                
}])
;
