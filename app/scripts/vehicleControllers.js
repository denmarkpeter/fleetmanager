'use strict';

angular.module('confusionApp')

.controller('VehiclesController', ['$scope', '$state', '$stateParams','vehicleFactory','groupFactory','locationFactory'
    , function($scope, $state, $stateParams,vehicleFactory,groupFactory,locationFactory) {
   
    $scope.showDetails = false;

    $scope.message = "Loading ...";

    $scope.showVehicles = false;
    $scope.vehicles = vehicleFactory.query({},
        function(response) {
            console.log("VEHICLES");
            $scope.vehicles = response;
            $scope.showVehicles = true;
        },
        function(response) {
            console.log("OHH ERROR");
            $scope.message = "Error: "+response.status + " " + response.statusText;
        }
    );

    $scope.toggleDetails = function() {
        $scope.showDetails = !$scope.showDetails;
    };
/*
    $scope.gotoEditVehicle = function(vehicleId) {
        console.log("Going to: ", vehicleId)
        $state.go('app.vehicle', {id: vehicleId, cameFrom: 'vehicles'});                        // manager stops looking at some person's trip
    };
*/
}])

.controller('VehicleController', ['$scope','$state','$stateParams', 'groupFactory','locationFactory','vehicleFactory','peopleFactory'
    , function($scope,$state,$stateParams,groupFactory,locationFactory,vehicleFactory,peopleFactory) {

    $scope.selectedLocation = null;
    $scope.inGroup = null;
    $scope.editing = false;

    function vehicleDateConverter(vehicle) {
      vehicle.lastMaintenanceDate = new Date(vehicle.lastMaintenanceDate);
      vehicle.nextMaintenanceDate = new Date(vehicle.nextMaintenanceDate);
      vehicle.carFirstRegistrated = new Date(vehicle.carFirstRegistrated);
    }

    function getGroups() {
            // get locations and groups
        groupFactory.query(
            function (response) {
                $scope.allGroups = response;
                $scope.showGroups = true;
                getLocations();
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
            });
    }

    function getLocations() {
            // get locations and groups
        locationFactory.query(
            function (response) {
                $scope.locations = response;
                $scope.showLocations = true;
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
            });
    }

    $scope.locationChanged = function() {
        $scope.groups = [];                        // find groups at selected location
        $scope.allGroups.forEach(function(group) {
            if (group.locationId._id == $scope.selectedLocation) {
                $scope.groups.push(group);
            }
        });
        var ifound = -1;                           // try to find present vehicle group selected locations groups
        for (var i = 0; i < $scope.groups.length; ++i){
            if ($scope.groups[i]._id == $scope.vehicle.inGroup._id) {
                ifound = i;
                break; 
            }
        }
        if (ifound !== -1)
            $scope.inGroup = $scope.vehicle.inGroup._id;  // don't autochange present vehicle group
        else if ( $scope.groups.length == 1 )     // autoselect group if only one group at location
            $scope.inGroup =  $scope.groups[0]._id;
    }

    getGroups();


    if (!$stateParams.id)
    {                                  // new vehicle
        $scope.editing = true;
        $scope.vehicle = {};
        $scope.showVehicle = true; 
    } else {                           // updating vehicle
        $scope.showVehicle = false;
        $scope.vehicle = vehicleFactory.get({ 
                id: $stateParams.id
        })
        .$promise.then(
            function (response) {
                $scope.vehicle = response;
                vehicleDateConverter($scope.vehicle);
                $scope.selectedLocation = $scope.vehicle.inGroup.locationId._id;
                $scope.locationChanged();
//                $scope.inGroup = $scope.vehicle.inGroup._id;
                $scope.showVehicle = true;
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
        });
    }

    $scope.saveVehicle = function (response) {
        $scope.vehicle.inGroup =  $scope.inGroup;
        if (!$stateParams.id) {                               // new vehicle
            vehicleFactory.save($scope.vehicle,
                function(data) {
                    $state.go('app.vehicles', {});
                },
                function(err) {
                    var errobject = err.data.error.errors;
                    for (var x in errobject) {
                        console.log(x + ': ' + errobject[x].message);
                    }
            });                
        } else {                                              // updating vehicle
            vehicleFactory.update({id: $stateParams.id}, $scope.vehicle,
                function(data) {
                    $state.go('app.vehicles', {});
                },
                function(err) {
                    var errobject = err.data.error.errors;
                    for (var x in errobject) {
                        console.log(x + ': ' + errobject[x].message);
                    }
            });
        };
        $scope.cancelEditVehicle();
    };

    $scope.groupChanged = function() {

    }

    $scope.editVehicle = function() {
        $scope.editing = true;
    }; 

    $scope.deleteVehicle = function() {              // we don not actually delete because some trips might have references
//         vehicleFactory.delete({id: $stateParams.id});
        $scope.cancelEditVehicle();
    }; 
    
    $scope.cancelEditVehicle = function() {
        if (!$stateParams.id) {                                   // how do I know where he/she came from?
            $state.go('app.vehicles', {});                        // manager stops looking at some person's trip
        } else {
            $state.go('app.vehicles', {});                        // manager stops looking at some person's trip
//                $state.go('app.persondash',{id: $scope.userId});   // person stops looking/editing own trips
        }
    };
}])
;
