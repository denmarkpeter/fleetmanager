'use strict';

angular.module('confusionApp')
.constant("baseURL", "https://fleetserver.herokuapp.com/")
//.constant("baseURL", "http://localhost:3000/")
.factory('menuFactory', ['$resource', 'baseURL', function ($resource, baseURL) {

        return $resource(baseURL + "dishes/:id", null, {
            'update': {
                method: 'PUT'
            }
        });

}])
.factory('peopleFactory',  ['$resource', 'baseURL', function($resource,baseURL) {  

        return $resource(baseURL + "people/:id", null, {
            'update': {
                method: 'PUT'
            }
        });

}])
.factory('peopleFactoryGroupManagers',  ['$resource', 'baseURL', function($resource,baseURL) {  

        return $resource(baseURL + "people/groupmanagers/:groupId", null, {
            'update': {
                method: 'PUT'
            }
        });

}])
.factory('commentFactory', ['$resource', 'baseURL', function ($resource, baseURL) {

        return $resource(baseURL + "dishes/:id/comments/:commentId", {id:"@Id", commentId: "@CommentId"}, {
            'update': {
                method: 'PUT'
            }
        });

}])

.factory('promotionFactory', ['$resource', 'baseURL', function ($resource, baseURL) {

    return $resource(baseURL + "promotions/:id", null, {
            'update': {
                method: 'PUT'
            }
        });

}])

.factory('corporateFactory', ['$resource', 'baseURL', function ($resource, baseURL) {


    return $resource(baseURL + "leadership/:id", null, {
            'update': {
                method: 'PUT'
            }
        });

}])

.factory('feedbackFactory', ['$resource', 'baseURL', function ($resource, baseURL) {


    return $resource(baseURL + "feedback/:id", null, {
            'update': {
                method: 'PUT'
            }
        });

}])

.factory('$localStorage', ['$window', function ($window) {
    return {
        store: function (key, value) {
            $window.localStorage[key] = value;
        },
        get: function (key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        remove: function (key) {
            $window.localStorage.removeItem(key);
        },
        storeObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key, defaultValue) {
            return JSON.parse($window.localStorage[key] || defaultValue);
        }
    }
}])

.factory('AuthFactory', ['$resource', '$http', '$localStorage', '$rootScope', '$window', 'baseURL', 'ngDialog',
              function($resource, $http, $localStorage, $rootScope, $window, baseURL, ngDialog){
    
    var authFac = {};
    var TOKEN_KEY = 'Token';
    var isAuthenticated = false;
    var username = '';
    var personId = '';
    var authToken = undefined;
    

  function loadUserCredentials() {
    console.log("LOADING USR CREDENTIALS");
    var credentials = $localStorage.getObject(TOKEN_KEY,'{}');
    if (credentials.username != undefined) {
        useCredentials(credentials);
    }
  }
 
  function storeUserCredentials(credentials) {
    $localStorage.storeObject(TOKEN_KEY, credentials);
    useCredentials(credentials);
  }
 
  function useCredentials(credentials) {
    isAuthenticated = true;
    username  = credentials.username;
    personId  = credentials.personId;
    authToken = credentials.token;
    // Set the token as header for your requests!
    $http.defaults.headers.common['x-access-token'] = authToken;
  }
 
  function destroyUserCredentials() {
    authToken = undefined;
    username = '';
    personId   = '';
    isAuthenticated = false;
    $http.defaults.headers.common['x-access-token'] = authToken;
    $localStorage.remove(TOKEN_KEY);
    // reset menu
    $rootScope.$broadcast('login:LogOut');
  }
     
    authFac.login = function(loginData) {
        
        $resource(baseURL + "users/login/")
        .save(loginData,
           function(response) {
              storeUserCredentials({username:loginData.username,personId:response.personId, token: response.token});
              $rootScope.$broadcast('login:Successful');
           },
           function(response){
              isAuthenticated = false;
            
              var message = '\
                <div class="ngdialog-message">\
                <div><h3>Login Unsuccessful</h3></div>' +
                  '<div><p>' +  response.data.err.message + '</p><p>' +
                    response.data.err.name + '</p></div>' +
                '<div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click=confirm("OK")>OK</button>\
                </div>'
            
                ngDialog.openConfirm({ template: message, plain: 'true'});
           }
        
        );

    };
    
    authFac.logout = function() {
        $resource(baseURL + "users/logout/").get(function(response){
        });
        destroyUserCredentials();
    };
/*    
    authFac.register = function(registerData) {
        console.log("register new user");
        $resource(baseURL + "users/register")
        .save(registerData,
           function(response) {
                    console.log("DONE: register new user");
              authFac.login({username:registerData.username, password:registerData.password});
            if (registerData.rememberMe) {
                $localStorage.storeObject('userinfo',
                    {username:registerData.username,personId:registerData.personId, password:registerData.password});
            }
           
              $rootScope.$broadcast('registration:Successful');
           },
           function(response){
            console.log("REGISTER UNSUCCESSFUL")
            console.log(response);
              var message = '\
                <div class="ngdialog-message">\
                <div><h3>Registration Unsuccessful</h3></div>' +
                  '<div><p>' +  response.data.err.message + 
                  '</p><p>' + response.data.err.name + '</p></div>';

                ngDialog.openConfirm({ template: message, plain: 'true'});

           }
        
        );
    };
*/    
    authFac.isAuthenticated = function() {
        return isAuthenticated;
    };
    
    authFac.getUsername = function() {
        return username;  
    };

    authFac.getUserId = function() {
        return personId;  
    };

    loadUserCredentials();
    
    return authFac;
    
}])

.factory('userFactory',  ['$resource', 'baseURL', function($resource,baseURL) {
// OBS TEST ONLY TEST ONLY
    return $resource(baseURL + "users/:id", null, {
        'update': {
            method: 'PUT'
        }
    });
}])

.factory('tripFactory',  ['$resource', 'baseURL', function($resource,baseURL) {

    return $resource(baseURL + "trips/:id", null, {
        'update': {
            method: 'PUT'
        }
    });
}])

.factory('tripFactoryPerson',  ['$resource', 'baseURL', function($resource,baseURL) {

    return $resource(baseURL + "trips/person/:personId", null, {
        'update': {
            method: 'PUT'
        }
    });
}])

.factory('tripFactoryPHS',  ['$resource', 'baseURL', function($resource,baseURL) {

    return $resource(baseURL + "trips/month/:monthNo", null, {
        'update': {
            method: 'PUT'
        }
    });
}])

.factory('reservationFactory',  ['$resource', 'baseURL', function($resource,baseURL) {

    return $resource(baseURL + "reservations/:id", null, {
        'update': {
            method: 'PUT'
        }
    });
}])

.factory('reservationFactoryPerson',  ['$resource', 'baseURL', function($resource,baseURL) {

    return $resource(baseURL + "reservations/person/:personId", null, {
        'update': {
            method: 'PUT'
        }
    });
}])

.factory('locationFactory',  ['$resource', 'baseURL', function($resource,baseURL) {

    return $resource(baseURL + "locations/:id", null, {
        'update': {
            method: 'PUT'
        }
    });
}])

.factory('groupFactory',  ['$resource', 'baseURL', function($resource,baseURL) {

    return $resource(baseURL + "groups/:id", null, {
        'update': {
            method: 'PUT'
        }
    });

}])

.factory('vehicleFactory',  ['$resource', 'baseURL', function($resource,baseURL) {

    return $resource(baseURL + "vehicles/:id", null, {
        'update': {
            method: 'PUT'
        }
    });

}])

.factory('persondashFactory',  ['$resource', 'baseURL', function($resource,baseURL) {

    return $resource(baseURL + "persondash/:id", null, {
        'update': {
            method: 'PUT'
        }
    });

}])


;