'use strict';

angular.module('confusionApp')

.controller('LoginController', ['$scope', '$filter','ngDialog', '$localStorage', 'AuthFactory', function ($scope, $filter,ngDialog, $localStorage, AuthFactory) {
    
    $scope.loginData = $localStorage.remove('userinfo');
    $scope.loginData = $localStorage.getObject('userinfo','{}');
    
    $scope.upper = function () {
        var upCase = $filter('uppercase');
        $scope.loginData.username = upCase($scope.loginData.username);
    };

    $scope.doLogin = function() {
        if($scope.rememberMe)
           $localStorage.storeObject('userinfo',$scope.loginData);
        AuthFactory.login($scope.loginData);

        ngDialog.close();
    };
            
    $scope.openRegister = function () {
        ngDialog.open({ template: 'views/register.html', scope: $scope, className: 'ngdialog-theme-default', controller:"RegisterController" });
    };
    
}])

.controller('RegisterController', ['$scope', 'ngDialog', '$localStorage', 'AuthFactory',
                           function ($scope, ngDialog, $localStorage, AuthFactory) {
    
    $scope.register={};
    $scope.loginData={};
    
    $scope.doRegister = function() {
        console.log('Doing registration', $scope.registration);
debugger
        AuthFactory.register($scope.registration);

        console.log('Registration Done', $scope.registration);
        
        ngDialog.close();

    };
}])

.controller('SelectPersonController', ['$scope', '$filter','ngDialog','peopleFactory',
                                function ($scope, $filter,ngDialog,peopleFactory) {
    
    $scope.upper = function () {
        var upCase = $filter('uppercase');
        $scope.loginData.username = upCase($scope.loginData.username);
    };

    $scope.showPeople = false;
    $scope.people = peopleFactory.query({isProspect: false},
        function(response) {
            $scope.people = response;
            $scope.showPeople = true;
/*            $scope.people.forEach(function(x){
                x.groups = x.inGroup.map(function(y){return y.name});
            })
            $scope.people.forEach(function(x){
                x.manages = x.managerOfGroups.map(function(y){return y.name});
            })
            $scope.select('all');
*/        },
        function(response) {
            $scope.message = "Error: "+response.status + " " + response.statusText;
    });

/*    $scope.selectedPersonChanged = function() {
        console.log($scope.selectedPerson)
    }
*/    
}])

.controller('PeopleController', ['$scope', '$stateParams', 'peopleFactory',
        function($scope, $stateParams, peopleFactory) {
 
    $scope.tab = '';
    $scope.filtText = '';
    $scope.showDetails = false;

    $scope.showSystemManager = false;
    $scope.message = "Loading ...";

    $scope.showPeople = false;
//            $scope.people = peopleFactory.query({ "inGroup": parseInt(1)},
    $scope.people = peopleFactory.query({},
        function(response) {
            $scope.people = response;
            $scope.showPeople = true;
            $scope.show = true;
            $scope.people.forEach(function(x){
                x.groups = x.inGroup.map(function(y){return y.name});
            })
            $scope.people.forEach(function(x){
                x.manages = x.managerOfGroups.map(function(y){return y.name});
                x.isManager = x.manages.length > 0;
            })
            $scope.select('all');
        },
        function(response) {
            $scope.message = "Error: "+response.status + " " + response.statusText;
    });

    $scope.toShow = $scope.people;
//    $scope.show = $scope.showPeople;


    $scope.select = function(setTab) {
        $scope.tab = setTab;
        
        if (setTab === 'prospects') {
            $scope.filtText = {isProspect: true}
        }
        else if (setTab === 'managers') {
            $scope.filtText = {isManager: true}
         }
        else if (setTab === 'topmanagers') {
            $scope.filtText = {isTopManager: true}
       }
        else 
        {
            $scope.filtText = {}
        }
    };

    $scope.isSelected = function (checkTab) {
        return ($scope.tab === checkTab);
    };

    $scope.toggleDetails = function() {
        $scope.showDetails = !$scope.showDetails;
    };

}])
.controller('PersonInfoController', ['$scope', '$state','$filter','$stateParams','peopleFactory','locationFactory','groupFactory','AuthFactory',
                    function($scope, $state,$filter,$stateParams,peopleFactory,locationFactory,groupFactory,AuthFactory) {

    $scope.message = "Loading ...";

    if(AuthFactory.isAuthenticated()) {
        $scope.loggedIn = true;
        $scope.username = AuthFactory.getUsername();
        $scope.userId   = AuthFactory.getUserId();
    }

    $scope.showPersonInfo= false;
    $scope.showLocations = false;
    $scope.showGroups = false;
    $scope.editing = false;
    $scope.selfie  = $stateParams.id === $scope.userId;

    $scope.upper = function () {
        var upCase = $filter('uppercase');
        $scope.person.username = upCase($scope.person.username);
    };

    // get locations and groups
    locationFactory.query(
        function (response) {
            $scope.locations = response;
            $scope.showLocations = true;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        });

    groupFactory.query(
        function (response) {
            $scope.groups = response;
            $scope.showGroups = true;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        });

    
    $scope.toggleEdit = function()
    {
        $scope.editing = !$scope.editing;
    }

    if (!$stateParams.id)
    {
        $scope.editing = true;
        $scope.person = {
            firstname: "",
            familyname: "",
            username: "",
            email: "",
            workLocation: "",
            isTopManager: false,
            inGroup: [],
            managerOfGroups: [],
            isProspect: true,
            isHistoric: false
        };
        $scope.showPerson = true;
    } else {  
        // overskriv person hvis person id er givet
        $scope.group = peopleFactory.get({
                id: $stateParams.id
            })
            .$promise.then(
                function (response) {
                    $scope.person = response;
                    $scope.showPersonInfo = true;
                    if ($scope.person.workLocation) {
                        $scope.workLocation = $scope.person.workLocation._id;
                    }
                    $scope.inGroup = $scope.person.inGroup.map(function(x){return x._id})  // fordi den allerede er udfyldt
                    $scope.managerOfGroups = $scope.person.managerOfGroups.map(function(x){return x._id})  // fordi den allerede er udfyldt
                    if ($scope.selfie)
                        $scope.user = $scope.person;
                    else
                    {
                        peopleFactory.query({_id: $scope.userId},
                            function(response) {
                                $scope.user = response;
                            },
                            function(response) {
                                $scope.message = "Error: "+response.status + " " + response.statusText;
                        });
                    }                
                },
                function (response) {
                    $scope.message = "Error: " + response.status + " " + response.statusText;
                }
            );
    } 


    $scope.savePersonalInfo = function (response) {
        if ($scope.workLocation) { 
            $scope.person.workLocation = $scope.workLocation;
        }
        $scope.person.inGroup          = $scope.inGroup;
//         $scope.person.managerOfGroups  = $scope.managerOfGroups;    // skal altid ændres via gruppen

        if (!$stateParams.id) {
            $scope.person.isTopManager = false;
            $scope.person.isProspect   = true;

            peopleFactory.save($scope.person,
                function(data) {
                    $state.go('app.home',{}); // will always be a prospect
                },
                function(err) {
                    var errobject = err.data.error.errors;
                    for (var x in errobject) {
                        console.log(x + ': ' + errobject[x].message);
                    }
            });                
        } else {
            peopleFactory.update({id: $stateParams.id}, $scope.person,
                function(data) {
                    if ($scope.prospect) {
                        if ($scope.selfie) {
                            $state.go('app.home', {});
                        } else {
                            $state.go('app.people', {});                           
                        }
                    } else {
                        if ($scope.selfie) {
                            $state.go('app.persondash',{id: $scope.userId});
                        } else {
                            $state.go('app.people', {});                           
                        }

                    }
                },
                function(err) {
                    console.log('Save failed.')
                    console.log('message: '+err.data.message);
                    var errobject = err.data.error.errors;
                    for (var x in errobject) {
                        console.log(x + ': ' + errobject[x].message);
                    }
                    console.log(err);
                });

        };
        $scope.cancelPersonalInfo();
    };

    $scope.cancelPersonalInfo = function () {
        if (!$stateParams.id) {
                $state.go('app.home', {});    // new person gave up registration
        } else if ($scope.prospect) {
            if ($scope.selfie) {
                $state.go('app.home', {});    // prospect stops looking at own info
            } else {
                $state.go('app.people', {});  // manager stops looking at prospect
            }
        } else {
            if ($scope.selfie) {

                $state.go('app.persondash',{id: $scope.userId});   // person stops looking at own personalInfo
            } else {
                $state.go('app.people', {});                         // manager stops looking at some personalInfo                          
            }
        }
    }
}])
.controller('PersonSignupController', ['$scope', '$state','$filter','$stateParams','locationFactory','peopleFactory','AuthFactory',
                    function($scope, $state,$filter,$stateParams,locationFactory,peopleFactory,AuthFactory) {

    $scope.message = "Loading ...";

    $scope.upper = function () {
        var upCase = $filter('uppercase');
        $scope.person.username = upCase($scope.person.username);
    };
    
    locationFactory.query(
    function (response) {
        $scope.locations = response;
        $scope.showLocations = true;
    },
    function (response) {
        $scope.message = "Error: " + response.status + " " + response.statusText;
    });

    $scope.person = {
        firstname: "",
        familyname: "",
        username: "",
        email: "",
//        workLocation: "",
        isTopManager: false,
        inGroup: [],
        managerOfGroups: [],
        isProspect: true,
        isHistoric: false
    };
    $scope.showPerson = true;

    $scope.locationChanged = function () {
        $scope.person.workLocation = $scope.workLocation;
    }
    
    $scope.savePersonalInfo = function (response) {
        $scope.person.isTopManager = false;
        $scope.person.isProspect   = true;

        peopleFactory.save($scope.person,
            function(data) {
                $state.go('app.home',{}); // will always be a prospect
            },
            function(err) {
                var errobject = err.data.error.errors;
                for (var x in errobject) {
                    console.log(x + ': ' + errobject[x].message);
                }
            });                
    };

    $scope.cancelPersonalInfo = function () {
        $state.go('app.home', {});    // new person gave up registration
    }
}])
;
