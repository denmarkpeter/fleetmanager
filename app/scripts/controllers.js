'use strict';

angular.module('confusionApp')

.controller('HeaderController', ['$scope', '$state', '$rootScope', 'ngDialog', 'AuthFactory','peopleFactory',
         function ($scope, $state, $rootScope, ngDialog, AuthFactory,peopleFactory) {

        
    $scope.openLogin = function () {
        ngDialog.open({ template: 'views/login.html', scope: $scope, className: 'ngdialog-theme-default', controller:"LoginController" });
    };
    
    $scope.logOut = function() {
       AuthFactory.logout();
        $scope.loggedIn = false;
        $scope.username = '';
        $scope.userId   = '';       
    };
    
    function setMenuStatus() {

        if (!$scope.userId) {
            $scope.menuType = -1;    // unknown
        } else {
            peopleFactory.get({id:$scope.userId})
            .$promise.then(
                function(response) {
                    var newperson = response;
                    if (newperson.isTopManager) {
                        $scope.menuType = 4;   // topManager
                    } else if (newperson.managerOfGroups.length > 0) {
                        $scope.menuType = 3;    // manager
                    } else if (newperson.isProspect) {
                        $scope.menuType = 1;    // prospect
                    } else {
                        $scope.menuType = 2;    // person
                    };
//                    $scope.menuType = 4;
                },
                function(response) {
                    $scope.message = "Error: "+response.status + " " + response.statusText;
            }
        );        
        }
    }

    $rootScope.$on('login:Successful', function () {
        $scope.loggedIn = AuthFactory.isAuthenticated();
        $scope.username = AuthFactory.getUsername();
        $scope.userId   = AuthFactory.getUserId();
        setMenuStatus("AUTH personId: ",$scope.userId)
        if ($scope.userId == null) {
            $state.go('app.home')
        } else {
            $state.go('app.persondash', {id:$scope.userId});
        }
    });
        
    $rootScope.$on('login:LogOut', function () {
        console.log("LOGGING OUT")
        $scope.loggedIn = false;
        $scope.username = '';
        $scope.userId = '';
        $scope.menuType =-1
        setMenuStatus();
        $state.go('app.home')
    });



    $rootScope.$on('registration:Successful', function () {
        $scope.loggedIn = AuthFactory.isAuthenticated();
        $scope.username = AuthFactory.getUsername();
        $scope.userId = AuthFactory.getUserId();
        $scope.setMenuStatus($scope.userId)
        $state.go('app.persondash', {id:$scope.userId});
    });
    
    $scope.stateis = function(curstate) {
       return $state.is(curstate);  
    };

    $scope.loggedIn = false;
    $scope.username = '';
    $scope.userId = '';
    $scope.menuType =-1

    if(AuthFactory.isAuthenticated()) {
        $scope.loggedIn = true;
        $scope.username = AuthFactory.getUsername();
        $scope.userId   = AuthFactory.getUserId();
    }

    $scope.setMenuFromUserType = function(personId) {
    debugger
        setMenuStatus();
    }
    setMenuStatus();
    
}])


.controller('PersonDashboardController', ['$scope', '$stateParams','peopleFactory','reservationFactory','reservationFactoryPerson', 'tripFactory', 'tripFactoryPerson','AuthFactory',
       function($scope, $stateParams,peopleFactory,reservationFactory,reservationFactoryPerson,tripFactory,tripFactoryPerson,AuthFactory) {

    $scope.userId = $stateParams.id;

    //console.log("PERSONDASH stateParams: ",$stateParams)
    // start tom gruppe

    $scope.showReservations   = false;
    $scope.showReservationsList   = true;
    $scope.showTrips = false;
    $scope.showTripsList = true;

    $scope.refreshReservations = function()
    {
        $scope.reservations = reservationFactoryPerson.query({ 
            personId: $scope.userId
        })
        .$promise.then(
            function(response) {
                $scope.reservations = response;
//                $scope.reservations.forEach(function(x){
//                    x.groups = x.inGroup.map(function(y){return y.name});
//            })

                $scope.showReservations = true;
            },
            function(response) {
                $scope.message = "Error: "+response.status + " " + response.statusText;
            }
        );        
    }
    $scope.refreshTrips = function()
    {
       $scope.showTrips = false;
        $scope.trips = tripFactoryPerson.query({ 
            personId: $scope.userId
        })
        .$promise.then(
            function(response) {
                $scope.trips = response;
                $scope.showTrips = true;
            },
            function(response) {
                $scope.message = "Error: "+response.status + " " + response.statusText;
            }
        );        
    }

    $scope.showPerson = false;
    peopleFactory.get({id:$scope.userId}) 
    .$promise.then(
        function(response) {
            $scope.person = response;
            $scope.showPerson = true;
//            var result = $scope.person.inGroup.map(function(x){return x._id})
        },
        function(response) {
            $scope.message = "Error: "+response.status + " " + response.statusText;
        }
    );        


    $scope.refreshReservations();
    $scope.refreshTrips();

    $scope.toggleReservations = function(){
      $scope.showReservationsList = !$scope.showReservationsList
    }

    $scope.toggleTrips = function(){
      $scope.showTripsList = !$scope.showTripsList
    }
     
}])
.controller('SeedController', ['$scope', '$stateParams','locationFactory','groupFactory','peopleFactory','vehicleFactory','reservationFactory', 'tripFactory','userFactory','AuthFactory',
       function($scope, $stateParams,locationFactory,groupFactory,peopleFactory,vehicleFactory,reservationFactory,tripFactory,userFactory,AuthFactory) {

    $scope.userId = $stateParams.id;


    $scope.locations = [];
    $scope.groups = [];
    $scope.people = [];
    $scope.vehicles = [];

    var purposes = ["Visit Customer","Visit Supplier","Visit Publisher","Visit Promotion Agency","Buy Stuff","Sell Stuff","Attend Conference",
                        "Meeting in Aarhus","Meeting in Odense","Meeting in Copenhagen","Inspection","Fetch guest"];
    var statuses = ["-","Accepted","Rejected"];


    var locationJSON = [
        {"name":"Odense","address":"Electric Engine Alley 1, Odense, DK-4444, Denmark"},
        {"name":"Copenhagen","address":"High Salary Street 10, Copenhagen, DK-3333, Denmark"},
        {"name":"Aarhus","address":"Windy Street 1010,Aarhus, DK-8000, Denmark"}
    ];

    var groupJSON = [
        {"name":"Odense-Guests","locationId":"Odense","historic":false},
        {"name":"Odense","locationId": "Odense","historic":false},
        {"name":"Copenhagen","locationId":"Copenhagen","historic":false},
        {"name":"Copenhagen-Guests","locationId":"Copenhagen","historic":false},
        {"name":"Aarhus","locationId":"Aarhus","historic":false}
    ];

    var personJSON = [
        {"firstname":"Zulu","familyname":"Zulusen","username":"ZULU","email":"zulu@zulu","workLocation":"Odense","isTopManager":true,"isProspect":false,"isHistoric":false,"managerOfGroups":["Odense-Guests","Odense","Copenhagen","Copenhagen-Guests","Aarhus"],"inGroup":["Odense-Guests","Odense"]},
        {"firstname":"Julie","familyname":"Hansen","username":"JULIE","email":"julie@zoomout.com","workLocation":"Odense","isTopManager":false,"isProspect":false,"isHistoric":false,"managerOfGroups":[],"inGroup":["Odense"]},
        {"firstname":"Anna","familyname":"Nielsen","username":"ANNA","email":"Anna@ZoomOut.com","workLocation":"Odense","isTopManager":false,"isProspect":false,"isHistoric":false,"managerOfGroups":[],"inGroup":["Odense"]},
        {"firstname":"Peter"  ,"familyname":"Jensen","username":"PETER"    ,"email":"peter@ZoomOut.com"  ,"workLocation":"Odense","isTopManager":false,"isProspect":false,"isHistoric":false,"telephone":"28400046","managerOfGroups":["Odense-Guests","Odense"],"inGroup":["Odense-Guests","Odense"]},
        {"firstname":"Jens"   ,"familyname":"Pedersen","username":"JENS"   ,"email":"jens@ZoomOut.com"   ,"workLocation":"Odense","isTopManager":false,"isProspect":false,"isHistoric":false,"telephone":"28400046","managerOfGroups":[]                          ,"inGroup":["Odense"]},
        {"firstname":"Kirsten","familyname":"Hansen"  ,"username":"KIRSTEN","email":"kirsten@ZoomOut.com","workLocation":"Odense","isTopManager":false,"isProspect":false,"isHistoric":false,"telephone":"28400046","managerOfGroups":["Odense-Guests","Odense"],"inGroup":["Odense"]},
        {"firstname":"Hanne","familyname":"Svenstrup","username":"HANNE","email":"hanne@ZoomOut.com","workLocation":"Odense","isTopManager":true,"isProspect":false,"isHistoric":false,"telephone":"28400046","managerOfGroups":[],"inGroup":["Odense"]},
        {"firstname":"Mette","familyname":"Christensen","username":"METTE","email":"mette@ZoomOut.com","workLocation":"Copenhagen","isTopManager":false,"isProspect":false,"isHistoric":false,"managerOfGroups":[],"inGroup":["Copenhagen"]},
        {"firstname":"Anne","familyname":"Sorensen","username":"ANNE","email":"anne@ZoomOut.com","workLocation":"Copenhagen","isTopManager":false,"isProspect":false,"isHistoric":false,"managerOfGroups":["Copenhagen","Copenhagen-Guests"],"inGroup":["Copenhagen"]},
        {"firstname":"Lars","familyname":"Larsen","username":"LARS","email":"lars@ZoomOut.com","workLocation":"Copenhagen","isTopManager":true,"isProspect":false,"isHistoric":false,"managerOfGroups":[],"inGroup":["Copenhagen","Copenhagen-Guests"]},
        {"firstname":"Michael","familyname":"Andersen","username":"MICHAEL","email":"michael@ZoomOut.com","workLocation":"Copenhagen","isTopManager":false,"isProspect":false,"isHistoric":false,"telephone":"28400046","managerOfGroups":[],"inGroup":[]},
        {"firstname":"Thomas","familyname":"Petersen","username":"THOMAS","email":"thomas@ZoomOut.com","workLocation":"Aarhus","isTopManager":false,"isProspect":false,"isHistoric":false,"telephone":"28400046","managerOfGroups":[],"inGroup":["Aarhus"]},
        {"firstname":"Helle","familyname":"Andersen","username":"HELLE","email":"helle@ZoomOut.com","workLocation":"Aarhus","isTopManager":false,"isProspect":false,"isHistoric":false,"telephone":"28400046","managerOfGroups":["Aarhus"],"inGroup":["Aarhus"]},
        {"firstname":"Henrik","familyname":"Rasmussen","username":"HENRIK","email":"henrik@ZoomOut.com","workLocation":"Copenhagen","isTopManager":false,"isProspect":false,"isHistoric":false,"telephone":"28400046","managerOfGroups":[],"inGroup":[]},
        {"firstname":"Susanne","familyname":"Joergensen","username":"SUSANNE","email":"susanne@ZoomOut.com","workLocation":"Aarhus","isTopManager":false,"isProspect":false,"isHistoric":false,"telephone":"28400046","managerOfGroups":[],"inGroup":["Aarhus"]},
        {"firstname":"qq","familyname":"qq","username":"QQ","email":"qq@qq","workLocation":"Aarhus","isTopManager":false,"isProspect":true,"isHistoric":false,"managerOfGroups":[],"inGroup":[]},
        {"firstname":"ss","familyname":"ss","username":"SS","email":"rr@rr","workLocation":"Odense","isTopManager":false,"isProspect":true,"isHistoric":false,"managerOfGroups":[],"inGroup":[]},
        {"firstname":"tt","familyname":"tt","username":"TT","email":"tt@tt","workLocation":"Odense","isTopManager":false,"isProspect":true,"isHistoric":false,"managerOfGroups":[],"inGroup":[]},
        {"firstname":"vv","familyname":"vv","username":"VV","email":"vv@vv","workLocation":"Odense","isTopManager":false,"isProspect":true,"isHistoric":false,"managerOfGroups":[],"inGroup":[]}
    ];

    var vehicleJSON = [
        {"plate":"AZK-12345","brand":"Tesla","model":"Model-C","seats":5,"inGroup":"Aarhus","description":"Mostly Spin, Only short distance","kmPresent":1000,"lastMaintenanceKm":1000,"lastMaintenanceDate":"2017-10-10T00:00:00.000Z","nextMaintenanceKm":10000,"nextMaintenanceDate":"2018-04-04T00:00:00.000Z","carFirstRegistrated":"2017-10-10T00:00:00.000Z"},
        {"plate":"AZZ-12345","brand":"Toyoto","model":"Insigna","seats":5,"inGroup":"Odense","description":"Ordinary car, very reliable","kmPresent":28000,"lastMaintenanceKm":15000,"lastMaintenanceDate":"2017-03-03T00:00:00.000Z","nextMaintenanceKm":30000,"nextMaintenanceDate":"2018-03-03T00:00:00.000Z","carFirstRegistrated":"2016-03-03T00:00:00.000Z"},
        {"plate":"AZU-12345","brand":"VW","model":"Golf","seats":4,"inGroup":"Copenhagen","description":"Eat up the road","kmPresent":40000,"lastMaintenanceKm":30000,"lastMaintenanceDate":"2017-01-22T00:00:00.000Z","nextMaintenanceKm":45000,"nextMaintenanceDate":"2018-01-22T00:00:00.000Z","carFirstRegistrated":"2015-01-22T00:00:00.000Z"},
        {"plate":"AZO-22345","brand":"Mercedes Benz","model":"520","seats":5,"inGroup":"Odense-Guests","description":"For guests and topmanagers","kmPresent":8000,"lastMaintenanceKm":0,"lastMaintenanceDate":"2017-02-20T00:00:00.000Z","nextMaintenanceKm":15000,"nextMaintenanceDate":"2018-02-20T00:00:00.000Z","carFirstRegistrated":"2018-02-20T00:00:00.000Z"},
        {"plate":"AXX-12345","brand":"Toyoto","model":"Corolla Coralla","seats":4,"inGroup":"Odense","description":"Small car, very reliable","kmPresent":22000,"lastMaintenanceKm":10000,"lastMaintenanceDate":"2017-04-04T00:00:00.000Z","nextMaintenanceKm":30000,"nextMaintenanceDate":"2018-04-04T00:00:00.000Z","carFirstRegistrated":"2016-04-04T00:00:00.000Z"},
        {"plate":"API-54321","brand":"Citroen","model":"Cactus","seats":5,"inGroup":"Odense","description":"A design beauty","lastMaintenanceDate":"2017-01-22T00:00:00.000Z","nextMaintenanceDate":"2018-01-22T00:00:00.000Z","carFirstRegistrated":"2016-04-04T00:00:00.000Z"},
        {"plate":"WOW-67890","brand":"Mercedes Benz","model":"720","seats":5,"inGroup":"Copenhagen-Guests","description":"King of the fleet","lastMaintenanceDate":"2017-01-22T00:00:00.000Z","nextMaintenanceDate":"2018-01-22T00:00:00.000Z","carFirstRegistrated":"2016-04-04T00:00:00.000Z"},
        {"plate":"UFO-00000","brand":"MacLaren","model":"F1-2017","seats":1,"inGroup":"Odense","description":"Flyes everywhere","lastMaintenanceDate":"2017-01-22T00:00:00.000Z","nextMaintenanceDate":"2018-01-22T00:00:00.000Z","carFirstRegistrated":"2016-04-04T00:00:00.000Z"}
    ];


    function deleteCollection(factory) {
        return new Promise(function(resolve,reject){
            factory.delete({})
            .$promise.then(response => resolve(response))
            .catch( err => {
                var errobject = err.data.error;
                console.log(err.data)
                for (var x in errobject) {
                    console.log(x + ': ' + errobject[x].message);
                }
                reject(err);
            });
        });
    }

    function deleteGroups() {
        return new Promise(function(resolve,reject){
            groupFactory.delete({})
            .$promise.then(response => resolve(response))
            .catch( err => {
                var errobject = err.data.error;
                console.log(err.data)
                for (var x in errobject) {
                    console.log(x + ': ' + errobject[x].message);
                }
                reject(err);
            });
        });
    }

    function deletePeople() {
        return new Promise(function(resolve,reject){
            peopleFactory.delete({})
            .$promise.then(response => resolve(response))
            .catch( err => {
                var errobject = err.data.error;
                console.log(err.data)
                for (var x in errobject) {
                    console.log(x + ': ' + errobject[x].message);
                }
                reject(err);
            });
        });
    }

    function deleteUsers() {
        return new Promise(function(resolve,reject){
            peopleFactory.delete({})
            .$promise.then(response => resolve(response))
            .catch( err => {
                var errobject = err.data.error;
                console.log(err.data)
                for (var x in errobject) {
                    console.log(x + ': ' + errobject[x].message);
                }
                reject(err);
            });
        });
    }

    function deleteVehicles() {
        return new Promise(function(resolve,reject){
            vehicleFactory.delete({})
            .$promise.then(response => resolve(response))
            .catch( err => {
                var errobject = err.data.error;
                console.log(err.data)
                for (var x in errobject) {
                    console.log(x + ': ' + errobject[x].message);
                }
                reject(err);
            });
        });
    }

    function deleteTrips() {
        return new Promise(function(resolve,reject){
            tripFactory.delete({})
            .$promise.then(response => resolve(response))
            .catch( err => {
                var errobject = err.data.error;
                console.log(err.data)
                for (var x in errobject) {
                    console.log(x + ': ' + errobject[x].message);
                }
                reject(err);
            });
        });
    }

    function deleteReservations() {
        return new Promise(function(resolve,reject){
            reservationFactory.delete({})
            .$promise.then(response => resolve(response))
            .catch( err => {
                var errobject = err.data.error;
                console.log(err.data)
                for (var x in errobject) {
                    console.log(x + ': ' + errobject[x].message);
                }
                reject(err);
            });
        });
    }

    function setLocations(resp,seedJSON) {
        return new Promise(function(resolve,reject){
            var ps = [];
            for (let i = 0; i < seedJSON.length; ++i) {
                ps[i] = locationFactory.save(seedJSON[i])
                .$promise.then(response => {return response}); 
            }

            Promise.all(ps)
                .then(res =>{$scope.locations = res; resolve(res)});
        });
    };

    function setGroups(locations,seedJSON){
        return new Promise(function(resolve,reject){
            for (var i = 0; i < seedJSON.length; ++i) {
                var locationIndex = locations.findIndex((x) => {return x.name === seedJSON[i]["locationId"]});
                if (locationIndex != -1)
                    seedJSON[i]["locationId"] = locations[locationIndex]._id; 
            }

            var ps = [];
            for (let i = 0; i < seedJSON.length; ++i) {
                ps[i] = groupFactory.save(seedJSON[i]) 
                    .$promise.then(response => {return response}); 
            }

            Promise.all(ps)
                .then(res =>{$scope.groups = res; resolve(res)});
        });
    };

    function setPeople(resp,groups,locations,seedJSON){
        return new Promise(function(resolve,reject){
            var personStart = 0
            var personCount = seedJSON.length;
            for (var i = personStart; i < personCount; ++i) {
                seedJSON[i]["password"] = seedJSON[i]["firstname"]+"123"; 

                var index = locations.findIndex((x) => {return x.name === seedJSON[i]["workLocation"]});
                if (index != -1)
                    seedJSON[i]["workLocation"] = locations[index]._id; 

                var opsamling = [];
                for (var j = 0; j < seedJSON[i]["managerOfGroups"].length; ++j) {
                    var index = groups.findIndex((x) => {return x.name === seedJSON[i]["managerOfGroups"][j]});
                    if (index != -1)
                        opsamling.push(groups[index]._id);
                }

                seedJSON[i]["managerOfGroups"] = opsamling; 
                opsamling = [];
                for (var j = 0; j < seedJSON[i]["inGroup"].length; ++j) {
                    var index = groups.findIndex((x) => {return x.name === seedJSON[i]["inGroup"][j]});
                    if (index != -1)
                        opsamling.push(groups[index]._id);
                }
                seedJSON[i]["inGroup"] = opsamling; 
            }

            var ps = [];
            for (let i = personStart; i < personCount; ++i) {
                ps[i] = peopleFactory.save(personJSON[i]) 
                    .$promise.then( response => { return response}); 
            }

            Promise.all(ps)
                .then(res =>{$scope.people = res;resolve(res)});
        });
    };

    function setVehicles(resp,groups,seedJSON){
        return new Promise(function(resolve,reject){
            for (var i = 0; i < seedJSON.length; ++i) {
                var index = groups.findIndex((x) => {return x.name === seedJSON[i]["inGroup"]});
                if (index != -1)
                    seedJSON[i]["inGroup"] = groups[index]._id; 
            }

            var ps = [];
            for (let i = 0; i < seedJSON.length; ++i) {
                ps[i] = vehicleFactory.save(seedJSON[i]) 
                    .$promise.then(response => {return response}); 
            }

            Promise.all(ps)
                .then(res =>{$scope.vehicles = res; resolve(res)});
        });
    };

    function randomIntFromInterval(min,max)
    {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

    function randomFloatFromInterval(min,max)
    {
        return Math.random()*(max-min+1)+min;
    }

    function findPeopleInGroup(groupId,people) {
        let vehiclePeople = [];
        for (let i = 0; i < people.length;++i) {
            if (-1 != people[i].inGroup.indexOf(groupId))
                vehiclePeople.push(people[i]);
        }
        return vehiclePeople;
    }

    function setRandomTripsReservations(resp,groups,vehicles,people,isTrips){
        let seedJSON = [];
        return new Promise(function(resolve,reject){
            let managerPct = 0.3;
            let periodLength = 60;     //days in the past
            let enDag = 24*60*60*1000;
            for (let i = 0; i < vehicles.length; ++i) {
                if (!vehicles[i].inGroup || vehicles[i].inGroup.length === 0)
                    continue;

                let vehiclePeople = findPeopleInGroup(vehicles[i].inGroup,people)
                if (vehiclePeople.length === 0)
                    continue;    

                let pctUsed        = randomFloatFromInterval(0.30,0.80);
                let today = new Date();
                var dateTo,lastDate;
                if (isTrips) {
                    dateTo = new Date().setDate(today.getDate() - periodLength-1)
                    lastDate = new Date();
                }
                else {
                    dateTo = new Date().setDate(today.getDate())
                    lastDate = new Date().setDate(today.getDate() + periodLength)
                }
                
                for (;dateTo < lastDate;) {
                    // nedsæt sandsynlighed for topmanager (fordi de er medlem af mange grupper)
                    // skal gøres inden der laves om på turens datoer
                    var newPerson = vehiclePeople[randomIntFromInterval(0,vehiclePeople.length-1)];
                    if (newPerson.isTopManager && Math.random() > managerPct)
                        continue;

                    let dateFrom = new Date().setTime(dateTo + enDag);
                    let dayCount = randomFloatFromInterval(1,5);
                    dateTo   = new Date().setTime(dateFrom + dayCount * enDag); // turlængde mellem 1 og 5 dage
                    // træk lod om turen skal afvikles eller bilen skal stå stille
                    if (Math.random() > pctUsed) 
                        continue;                // bilen står stille

                    var newTrip = {};
                    newTrip["dateFrom"]    = String(new Date(dateFrom));
                    newTrip["dateTo"]      = String(new Date(dateTo));
                    newTrip["purpose"]     = purposes[randomIntFromInterval(0,purposes.length-1)];
                    newTrip["personCount"] = randomIntFromInterval(1,vehicles[i].seats);
                    newTrip["personId"]    = newPerson._id;
                    newTrip["vehicleId"]   = vehicles[i]._id;
                    if (isTrips) {
                        newTrip["tripLength"] = 100 + 150*randomIntFromInterval(0,dayCount-1);
                        let statusRnd         = Math.random();
                        if (statusRnd > 0.9)
                            newTrip["status"]  = statuses[2];
                        else if  (statusRnd > 0.3)
                            newTrip["status"]  = statuses[1];
                        else
                            newTrip["status"]  = statuses[0];
                    }
                    seedJSON.push(newTrip);
                }
            }
            var factory;
            if (isTrips)
                factory = tripFactory;
            else 
                factory = reservationFactory;

            var ps = [];
            for (let i = 0; i < seedJSON.length; ++i) {
                ps[i] = factory.save(seedJSON[i]) 
                    .$promise.then(response => {return response}); 
            }

            Promise.all(ps)
                .then(res =>{resolve(res)});
        });
    };


    $scope.insertAll = function()
    {
        deleteCollection(locationFactory)
        .then(deleteCollection(groupFactory))
        .then(deleteCollection(userFactory))
        .then(deleteCollection(peopleFactory))
        .then(deleteCollection(vehicleFactory))
        .then(deleteCollection(tripFactory))
        .then(deleteCollection(reservationFactory))
        .then(function(resp){return setLocations(resp,locationJSON)})
        .then(function(resp){return setGroups(resp,groupJSON)})
        .then(function(resp){return setVehicles(resp,$scope.groups,vehicleJSON)})
        .then(function(resp){return setPeople(resp,$scope.groups,$scope.locations,personJSON)})
        .then(function(resp){return setRandomTripsReservations(resp,$scope.groups,$scope.vehicles,$scope.people,true)})
        .then(function(resp){return setRandomTripsReservations(resp,$scope.groups,$scope.vehicles,$scope.people,false)})
    }

    $scope.insertAll();
}])
;
