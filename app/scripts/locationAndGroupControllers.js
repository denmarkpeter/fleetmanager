'use strict';

angular.module('confusionApp')

.controller('LocationsController', ['$scope', '$state', '$stateParams', 'ngDialog', 'locationFactory',
        function($scope,$state, $stateParams,ngDialog, locationFactory) {

    $scope.message = "Loading ..."; 

    $scope.showLocations = false;
    locationFactory.query(
        function (response) {
            $scope.locations = response;
            $scope.showLocations = true;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
    });

    $scope.openEditLocation = function (id) {
        $scope.oldstate= $state;
        ngDialog.open({ template: 'views/location.html', scope: $scope, className: 'ngdialog-theme-default', controller:"LocationController" });
    };
}])

.controller('LocationController', ['$scope', '$state', '$stateParams', 'ngDialog', 'locationFactory','groupFactory',
                 function ($scope,$state,$stateParams, ngDialog, locationFactory,groupFactory) {

    $scope.showGroupsList = true;
    $scope.editing = false;

    $scope.showGroups = false;
    $scope.refreshGroups = function()
    {
        $scope.groupsOnLocation = groupFactory.query({ 
            locationId: $stateParams.id
        })
        .$promise.then(
            function(response) {
                $scope.groupsOnLocation = response;
                $scope.showGroups = true;
            },
            function(response) {
                $scope.message = "Error: "+response.status + " " + response.statusText;
            }
        );        
    }

    if (!$stateParams.id)
    {
        $scope.editing = true;
        $scope.location = {
            name: "",
            address: ""
        };
        $scope.showLocation = true;

        $scope.saveLocation = function () {
            locationFactory.save($scope.location);
            $scope.location = {
              name: "",
              address: ""
            };
            $scope.cancelEditLocation();
        };
    } else {
        $scope.showLocation = false;

        $scope.location = locationFactory.get(
            { 
                id: $stateParams.id
            }
        )
        .$promise.then(
            function (response) {
                $scope.location = response;
                $scope.refreshGroups();
                $scope.showLocation = true;
            },
            function (response) {
                $scope.message = "Error: " + response.status + " " + response.statusText;
            }
        );

        $scope.saveLocation = function() {
            locationFactory.update({id: $scope.location._id}, $scope.location);
            $scope.cancelEditLocation();
        }; 

        $scope.deleteLocation = function() {
//         locationFactory.delete({id: $stateParams.id});
        $scope.cancelEditLocation();
    }; 
    
    $scope.editLocation = function() {
        $scope.editing = true;
    }; 

    $scope.cancelEditLocation = function() {
        $state.go('app.locations', {});                        // manager stops looking at some person's trip
    };

    } 
}])

.controller('GroupsController', ['$scope', '$stateParams', 'groupFactory','locationFactory', 'vehicleFactory','peopleFactory','peopleFactoryGroupManagers'
    , function($scope, $stateParams, groupFactory,locationFactory, vehicleFactory,peopleFactory,peopleFactoryGroupManagers) {

    $scope.message = "Loading ...";

    $scope.showGroups = false;
    $scope.groups = groupFactory.query({},
        function(response) {
            $scope.groups = response;
            for (let i = 0; i < $scope.groups.length;++i) {
                peopleFactory.query({inGroup: $scope.groups[i]._id},
                    function(response) {
                        $scope.groups[i].memberCount = response.length;
                 },
                    function(response) {
                        $scope.message = "Error: "+response.status + " " + response.statusText;
                });
            };
            $scope.groups.forEach(function(x){
                x.managers = [];
                    peopleFactoryGroupManagers.query({groupId: x._id},
                    function(response) {
                        if (response.length >0){
                            response.forEach(function(y) {x.managers.push(y.firstname+" "+ y.familyname)});
                            $scope.showGroups = true;
                        }
                     },
                    function(response) {
                        console.log("ERROR managers search")
                        console.log(response)
                        $scope.message = "Error: "+response.status + " " + response.statusText;
                });

            });

            $scope.groups.forEach(function(x){
                vehicleFactory.query({inGroup: x._id},
                    function(response) {
                        x.vehicleCount = response.length;
                 },
                    function(response) {
                        $scope.message = "Error: "+response.status + " " + response.statusText;
                });
            });

            $scope.showGroups = true;
        },
        function(response) {
            $scope.message = "Error: "+response.status + " " + response.statusText;
        }); 

}])


.controller('AssignManagerController', ['$scope', '$state', '$stateParams', 'groupFactory','peopleFactory','peopleFactoryGroupManagers'
    , function($scope, $state, $stateParams, groupFactory,peopleFactory,peopleFactoryGroupManagers) {

    $scope.message = "Loading ...";
    $scope.candidateId = $stateParams.personId;

    function initFirst() {
        // slå personen op
        $scope.showGroups = false;
        peopleFactory.get({id:$scope.candidateId})
        .$promise.then(
            function(response) {
                $scope.candidate = response;
                $scope.groups = groupFactory.query({},
                    function(response) {
                        $scope.groups = response;
                        $scope.groups.forEach(function(x){
                            x.managers  = [];
                            x.isManager = [];
                            peopleFactoryGroupManagers.query({groupId: x._id},
                                function(response) {
                                    let isManager = false;
                                    for (let i = 0; i < response.length; ++i) {
                                        x.managers.push(response[i].firstname+" "+ response[i].familyname);
                                        if (response[i]._id === $scope.candidateId)
                                            isManager = true;
                                    }
                                    x.isManager.push(isManager);
                                 },
                                function(response) {
                                    console.log("ERROR managers search")
                                    console.log(response)
                                    $scope.message = "Error: "+response.status + " " + response.statusText;
                            });
                        });
                        $scope.showGroups = true;
                    },
                    function(response) {
                        $scope.message = "Error: "+response.status + " " + response.statusText;
                });
            },
            function(response) {
                $scope.message = "Error: "+response.status + " " + response.statusText;
        });
    }

    function updatePerson() {
        peopleFactory.update({id: $scope.candidateId}, $scope.candidate,
            function(data) {
                console.log("Person is updated");
                initFirst()
            },
            function(err) {
                console.log('Person update failed');
        });        
    }

    $scope.groupAssignAsManager = function(groupId) {
        if ($scope.candidate && $scope.candidate.managerOfGroups.indexOf(groupId) === -1)  {
            $scope.candidate.managerOfGroups.push(groupId);
            updatePerson();
        }
    }

    $scope.groupRemoveAsManager = function(groupId) {
        if ($scope.candidate) {
            for (let i = 0; i < $scope.candidate.managerOfGroups.length; ++i ) {
                if ($scope.candidate.managerOfGroups[i]._id == groupId) {
                    $scope.candidate.managerOfGroups.splice(i,1);
                    updatePerson();
                    break;
                }
            }
        }
    }

    $scope.cancelAssignManager = function () {
        $state.go('app.person', {id: $scope.candidateId});    // new person gave up registration
    }

    initFirst();
    
}])


.controller('GroupController', ['$scope','$state','$stateParams','ngDialog', 'groupFactory','locationFactory', 'vehicleFactory','peopleFactory','peopleFactoryGroupManagers'
    , function($scope,$state,$stateParams,ngDialog,groupFactory,locationFactory,vehicleFactory,peopleFactory,peopleFactoryGroupManagers) {

    $scope.editing = false;
    $scope.toggleEdit = function() {
        $scope.editing = !$scope.editing;
    }
    $scope.managers = [];

    var dialog;

    // start tom gruppe
    $scope.showGroup = true;
    $scope.group = {
        name: "",
        locationId: "",
        historic: false
    };

    $scope.showPeopleList   = false;
    $scope.showVehiclesList = false;
    $scope.refreshPeople = function()
    {
        $scope.peopleInGroup = peopleFactory.query({ 
            inGroup: $stateParams.id
        })
        .$promise.then(
            function(response) {
                $scope.peopleInGroup = response;
                $scope.peopleInGroup.forEach(function(x){
                    x.groups = x.inGroup.map(function(y){return y.name});
            })

                $scope.showPeople = true;
            },
            function(response) {
                $scope.message = "Error: "+response.status + " " + response.statusText;
            }
        );        
    }
    $scope.refreshVehicles = function()
    {
       $scope.showVehicles = false;
        $scope.vehiclesInGroup = vehicleFactory.query({ 
            inGroup: $stateParams.id
        })
        .$promise.then(
            function(response) {
                $scope.vehiclesInGroup = response;
                $scope.showVehicles = true;
            },
            function(response) {
                $scope.message = "Error: "+response.status + " " + response.statusText;
            }
        );        
    }

    $scope.refreshManagers = function() {
        if (!$stateParams.id) return;    // new group

        peopleFactoryGroupManagers.query({groupId: $stateParams.id},
            function(response) {
                $scope.managers = [];
                if (response.length >0){
                    response.forEach(function(y) {$scope.managers.push(" "+y.firstname+" "+ y.familyname)});
                }
             },
            function(response) {
                console.log("ERROR managers search")
                console.log(response)
                $scope.message = "Error: "+response.status + " " + response.statusText;

        });
    }


    $scope.showLocations = false;
    locationFactory.query(
        function (response) {
            $scope.locations = response;
            $scope.showLocations = true;

            if (!$stateParams.id)
            {
                $scope.editing = true;
            }
            else
            {   // overskriv gruppe hvis gruppe id er givet
                $scope.showGroup = false; 
                $scope.group = groupFactory.get({
                        id: $stateParams.id
                    })
                    .$promise.then(
                        function (response) {
                            $scope.group = response;

                            $scope.refreshPeople();
                            $scope.refreshVehicles();
                            $scope.refreshManagers();

                            $scope.showGroup = true;
                        },
                        function (response) {
                            $scope.message = "Error: " + response.status + " " + response.statusText;
                        }
                    );
            } 

        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        });


    $scope.saveGroup = function (response) {
        if (!$stateParams.id) {
            groupFactory.save($scope.group,
                function(data) {
                    $state.go('app.groups', {});
                },
                function(err) {
                    var errobject = err.data.error.errors;
                    for (var x in errobject) {
                        console.log(x + ': ' + errobject[x].message);
                    }
            });                
        } else {
            groupFactory.update({id: $stateParams.id}, $scope.group,
                function(data) {
                    $state.go('app.groups', {});
                },
                function(err) {
                    var errobject = err.data.error.errors;
                    for (var x in errobject) {
                        console.log(x + ': ' + errobject[x].message);
                    }
            });
        };
        $scope.cancelEditGroup();
    };

    $scope.togglePersons = function(){
      $scope.showPeopleList = !$scope.showPeopleList
    }

    $scope.toggleVehicles = function(){
      $scope.showVehiclesList = !$scope.showVehiclesList
    }

    $scope.editGroup = function() {
        $scope.editing = true;
    }; 

    $scope.cancelEditGroup = function() {
        $state.go('app.groups', {});                        // manager stops looking at some person's trip
    };
     
}])
;