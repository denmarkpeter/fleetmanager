'use strict';

angular.module('confusionApp', ['ui.router','ngResource','ngDialog'])
.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
        
            // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller  : 'HeaderController'
                    },
                    'content': {
                        templateUrl : 'views/home.html',
                        controller  : 'HomeController'   //'IndexController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                }

            })
        
            // route for the home page
            .state('app.home', {
                url:'home',
                views: {
                    'content@': {
                        templateUrl : 'views/home.html',
                        controller  : 'HomeController'                  
                    }
                }
            })
        
            .state('app.aboutus', {
                url:'aboutus',
                views: {
                    'content@': {
                        templateUrl : 'views/aboutus.html',
                        controller  : 'AboutController'                  
                    }
                }
            })

            // route for the contactus page
            .state('app.contactus', {
                url:'contactus',
                views: {
                    'content@': {
                        templateUrl : 'views/contactus.html',
                        controller  : 'ContactController' //'ContactController' // 'SeedController'                   
                    }
                }
            })

           .state('app.people', {
                url: 'people',
                views: {
                    'content@': {
                        templateUrl : 'views/people.html',
                        controller  : 'PeopleController'
                    }
                }
            })
            .state('app.person', {
                url:'people/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/personinfo.html',
                        controller  : 'PersonInfoController'                  
                    }
                }
            })

            .state('app.signup', {
                url:'signup/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/personsignup.html',
                        controller  : 'PersonSignupController'                  
                    }
                }
            })

           .state('app.locations', {
                url: 'locations',
                views: {
                    'content@': {
                        templateUrl : 'views/locations.html',
                        controller  : 'LocationsController'
                    }
                }
            })
            
            .state('app.location', {
                url: 'locations/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/location.html',
                        controller  : 'LocationController'
                    }
                }
            })
            .state('app.trips', {
                url: 'trips',
                views: {
                    'content@': {
                        templateUrl : 'views/trips.html',
                        controller  : 'TripsController'
                    }
                }
            })
            
            .state('app.trip', {
                url: 'trips/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/trip.html',
                        controller  : 'TripController'
                    }
                }
            })
            
            .state('app.reservations', {
                url: 'reservations',
                views: {
                    'content@': {
                        templateUrl : 'views/reservations.html',
                        controller  : 'ReservationsController'
                    }
                }
            })
                       
            .state('app.reservation', {
                url: 'reservations/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/reservation.html',
                        controller  : 'ReservationController'
                    }
                }
            })
                       
            .state('app.groups', {
                url: 'groups',
                views: {
                    'content@': {
                        templateUrl : 'views/groups.html',
                        controller  : 'GroupsController'
                    }
                }
            })
            
           .state('app.group', {
                url: 'groups/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/group.html',
                        controller  : 'GroupController'
                    }
                }
            })
            
            .state('app.assignManager', {
                url: 'groups/managers/:personId',
                views: {
                    'content@': {
                        templateUrl : 'views/assignmanager.html',
                        controller  : 'AssignManagerController'
                    }
                }
            })

            .state('app.vehicles', {
                url: 'vehicles',
                views: {
                    'content@': {
                        templateUrl : 'views/vehicles.html',
                        controller  : 'VehiclesController'
                    }
                }
            })

            .state('app.vehiclesPHS', {
                url: 'trips/month/:monthNo',
                views: {
                    'content@': {
                        templateUrl : 'views/vehiclesPHS.html',
                        controller  : 'TripsControllerPHS'
                    }
                }
            })

            .state('app.vehicle', {
                url: 'vehicles/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/vehicle.html',
                        controller  : 'VehicleController',
                        params: { cameFrom: 'vehicles' }                
                    }
                }
            })
    
            .state('app.persondash', {
                url:'persondash/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/persondash.html',
                        controller  : 'PersonDashboardController',
/*                        params: {
                                personId: null
                            }                
*/                    }
                }
 
            })


        $urlRouterProvider.otherwise('/');
    })
;
