
var express = require('express'),
     http = require('http');

var cors = require('cors');

var app = express();
//app.use(cors({origin: '*'}));


var server = http.createServer(app);

var hostname = 'localhost';
var port = process.env.PORT || 3000;

app.use(express.static(__dirname + ''));
var options = {
  index: "index.html"
};
app.use('/', express.static('app', options));


server.listen (port,function () {
	  var host = server.address().address;
  var port = server.address().port;

  console.log('my app is listening at http://%s:%s', host, port);

});
